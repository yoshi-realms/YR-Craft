//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package tk.yoshirealms.craft;

import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.configuration.Configuration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin {
    private static main Plugin;
    private static Configuration config;
    private static Server server;

    public main() {
    }

    public void onEnable() {
        Plugin = this;
        config = getConfig();
        server = getServer();
        load();
        this.saveDefaultConfig();
    }

    public void onDisable() {
    }

    private static void load() {

        for (Object obj : config.getList("Recipes")) {
            String str = (String) obj;
            String item0_string = str.split(",")[0];
            Material item0_material = Material.getMaterial(item0_string.split(":")[0]);
            short item0_damage = Short.parseShort(item0_string.split(":")[1]);
            ItemStack item0 = new ItemStack(item0_material);
            String item1_string = str.split(",")[1];
            Material item1_material = Material.getMaterial(item1_string.split(":")[0]);
            Byte item1_damage = Byte.parseByte(item1_string.split(":")[1]);
            String item2_string = str.split(",")[2];
            Material item2_material = Material.getMaterial(item2_string.split(":")[0]);
            Byte item2_damage = Byte.parseByte(item2_string.split(":")[1]);
            String item3_string = str.split(",")[3];
            Material item3_material = Material.getMaterial(item3_string.split(":")[0]);
            Byte item3_damage = Byte.parseByte(item3_string.split(":")[1]);
            String item4_string = str.split(",")[4];
            Material item4_material = Material.getMaterial(item4_string.split(":")[0]);
            Byte item4_damage = Byte.parseByte(item4_string.split(":")[1]);
            String item5_string = str.split(",")[5];
            Material item5_material = Material.getMaterial(item5_string.split(":")[0]);
            Byte item5_damage = Byte.parseByte(item5_string.split(":")[1]);
            String item6_string = str.split(",")[6];
            Material item6_material = Material.getMaterial(item6_string.split(":")[0]);
            Byte item6_damage = Byte.parseByte(item6_string.split(":")[1]);
            String item7_string = str.split(",")[7];
            Material item7_material = Material.getMaterial(item7_string.split(":")[0]);
            Byte item7_damage = Byte.parseByte(item7_string.split(":")[1]);
            String item8_string = str.split(",")[8];
            Material item8_material = Material.getMaterial(item8_string.split(":")[0]);
            Byte item8_damage = Byte.parseByte(item8_string.split(":")[1]);
            String item9_string = str.split(",")[9];
            Material item9_material = Material.getMaterial(item9_string.split(":")[0]);
            Byte item9_damage = Byte.parseByte(item9_string.split(":")[1]);
            item0.setDurability(item0_damage);
            ShapedRecipe recipe = new ShapedRecipe(item0);
            recipe.shape("ABC", "DEF", "GHI");
            MaterialData item1_data = new MaterialData(item1_material, item1_damage);
            MaterialData item2_data = new MaterialData(item2_material, item2_damage);
            MaterialData item3_data = new MaterialData(item3_material, item3_damage);
            MaterialData item4_data = new MaterialData(item4_material, item4_damage);
            MaterialData item5_data = new MaterialData(item5_material, item5_damage);
            MaterialData item6_data = new MaterialData(item6_material, item6_damage);
            MaterialData item7_data = new MaterialData(item7_material, item7_damage);
            MaterialData item8_data = new MaterialData(item8_material, item8_damage);
            MaterialData item9_data = new MaterialData(item9_material, item9_damage);
            recipe.setIngredient('A', item1_data);
            recipe.setIngredient('B', item2_data);
            recipe.setIngredient('C', item3_data);
            recipe.setIngredient('D', item4_data);
            recipe.setIngredient('E', item5_data);
            recipe.setIngredient('F', item6_data);
            recipe.setIngredient('G', item7_data);
            recipe.setIngredient('H', item8_data);
            recipe.setIngredient('I', item9_data);
            server.addRecipe(recipe);
        }

    }
}
